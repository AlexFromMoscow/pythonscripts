import mysql.connector
from mysql.connector import errorcode

# If connection will be success, it returned "You are successfully connected to your database!"
try:
  cnx = mysql.connector.connect(user="alex",
                                password="Barhatov0206$",
                                host="192.168.228.128",
                                database="employees")
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)
else:
  print("You are successfully connected to your database!")

mycursor = cnx.cursor()

#Execute INSERT INTO query
query = "INSERT INTO employees (emp_no, birth_date, first_name, last_name, gender, hire_date) VALUES (%s, %s, %s, %s, %s, %s)"
values = ("500000", "1999-02-06", "Alexander", "Barkhatov", "M", "2021-01-28")
mycursor.execute(query, values)

cnx.commit()

print(mycursor.rowcount, "Records inserted")






