name = input("What is your name? \n");
age = int(input("How old are you? \n"));

print ("Your name is " + name);
print ("You are " + str(age) + " years old");

if age <= 18:
    print ("You are not 18 years old!")
else:
    print("You are 18 or older!");

#Another piece of code
number = 10;
while number < 20:
    print(number)
    number = number + 5;