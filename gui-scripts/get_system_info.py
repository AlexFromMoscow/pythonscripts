import os
import socket
import datetime
import platform
import getpass
from uuid import getnode as get_mac
from tkinter import *

root = Tk()
root.title("System INFO")
root.geometry("300x80")

name = getpass.getuser()
ip = socket.gethostbyname(socket.getfqdn())
mc = get_mac()
os = platform.uname()

print(name)
print(ip)
print(mc)
print(os)