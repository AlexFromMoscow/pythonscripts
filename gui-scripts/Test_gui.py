import os
import socket
import getpass
import datetime
import platform
import tkinter as tk
from tkinter import messagebox
from uuid import getnode as get_mac

window = tk.Tk()
window.title("System information")
window.geometry("600x200")

def showinput1():
    global button1
    messagebox.showinfo("User", user)

def showinput2():
    messagebox.showinfo("IP", ip)

def showinput3():
    messagebox.showinfo("MAC", mac)

def showinput4():
    messagebox.showinfo("OS", os)


button1 = tk.Button(
    text = "Show current user",
    width = 15,
    height = 2,
    command = showinput1
)

button2 = tk.Button(
    text = "Show IP address",
    width = 15,
    height = 2,
    command = showinput2
)

button3 = tk.Button(
    text = "Show MAC address",
    width = 15,
    height = 2,
    command = showinput3
)

button4 = tk.Button(
    text = "Show OS details",
    width = 15,
    height = 2,
    command = showinput4
)

#Place all 4 buttons on the left side
button1.grid(row = 1, column = 0)
button2.grid(row = 2, column = 0)
button3.grid(row = 3, column = 0)
button4.grid(row = 4, column = 0)

user = getpass.getuser()
ip = socket.gethostbyname(socket.getfqdn())
mac = get_mac()
os = platform.uname()

window.mainloop()